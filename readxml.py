import xmltodict
import sys
import os

try: 
    os.remove('status.html')
except OSError:
    pass

file_object = open(sys.argv[2], 'w')

with open(sys.argv[1]) as fd:
    doc = xmltodict.parse(fd.read())

def status(status):
    print("===")
    print(status)
    file_object=open('status.html','w')
    if status=='t' :
        file_object.write("</br>status=" + doc['builds']['build']['@status'] + "</br>")
    else:
        file_object.write("</br><font color='red' size='100'>status=FAILURE</font></br>")
    file_object.close()


(lambda x :  status('t') if x=='SUCCESS' else status('f'))(doc['builds']['build']['@status'] )
file_object.write("</br>status=" + doc['builds']['build']['@status'] + "</br>")
file_object.write("</br>id=" + doc['builds']['build']['@id'] + "</br>")
file_object.write("</br>webUrl=<a href='" + doc['builds']['build']['@webUrl'] + "' >" + doc['builds']['build']['@webUrl'] +"</a></br>")
file_object.write("</br>buildTypeId=" + doc['builds']['build']['@buildTypeId'] + "</br>")
file_object.close()

print ( doc['builds']['build'])
