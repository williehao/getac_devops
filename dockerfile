FROM continuumio/anaconda3:latest
RUN mkdir /work
WORKDIR /work
RUN conda update -n base -c defaults conda -y
RUN conda update conda -y
RUN conda update anaconda -y
RUN conda create --name teamcity2 python=3.7 -y
RUN /bin/bash -c activate teamcity2
RUN conda info | grep -i 'base environment'
RUN pip install xmltodict
